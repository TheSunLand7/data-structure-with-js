//Binary Search Tree
//         10
//     8       20
//   6  12   17  50

class Node {
    constructor(item) {
        this.value = item;
        this.left = null;
        this.right = null;
    }
}
class MyTree {
    constructor() {
        this.root = null;
    }

    insert(item) {
        const newNode = new Node(item);
        if (this.root === null) this.root = newNode;
        else {
            let temp = this.root;
            let run = true;
            while (run) {
                if (newNode.value < temp.value) {
                    if (!temp.left) {
                        temp.left = newNode;
                        return this;
                    }
                    temp = temp.left;
                } else {
                    if (!temp.right) {
                        temp.right = newNode;
                        return this;
                    }
                    temp = temp.right;
                }
            }
        }
    }
    search(item) {
        let temp = this.root;
        let run = true;
        if (temp !== null) {
            while (run) {
                if (item === temp.value) {
                    return "Item found: " + temp.value;
                } else if (item < temp.value) {
                    if (temp.left) {
                        temp = temp.left;
                        if (item === temp.value) {
                            return `Item ${item} found!`;
                        }
                    } else return `Item ${item} not found`;
                } else {
                    if (temp.right) {
                        temp = temp.right;
                        if (item === temp.value) {
                            return `Item ${item} found!`;
                        }
                    } else return `Item ${item} not found`;
                }
            }
        }

    }
    delete() {
        // Pending
    }
}

const myTree = new MyTree();
myTree.insert(10);
myTree.insert(8);
myTree.insert(20);
myTree.insert(6);
myTree.insert(12);
myTree.insert(17);
myTree.insert(50);
console.log(myTree);
