/* 
const fruit = ["Apple", "Banana", "Pineapple"];
fruit.forEach((item, index, array) => {
    console.log(item, index);
});
*/

class MyArray {
    constructor() {
        this.length = 0;
        this.data = {};
    }

    MyGet(index) {
        return this.data[index];
    }

    myPush(item) {
        this.data[this.length] = item;
        this.length++;
        return this.data;
    }

    myPop() {
        if (this.length === 0) console.log("Cannot delete empty array");
        else {
            let lastItem = this.data[this.length - 1];
            delete this.data[this.length - 1];
            this.length--;
            return lastItem;
        }
    }

    delete(index) {
        let item = this.data[index];
        this.shiftIndex(index);
        return item;
    }
    shiftIndex(index) {
        let item = this.data[index];
        for (let i = index; i < this.length - 1; i++) {
            this.data[i] = this.data[i + 1];
        }
        delete this.data[this.length - 1];
        this.length--;
    }

    myShift() {
        if (this.length === 0) console.log("Cannot delete empty array");
        else return this.delete(0);
    }
    myUnShift(item) {
        this.myPush(item);
        for (let i = (this.length - 1); i > 0; i--) {
            this.data[i] = this.data[i - 1];
        }
        this.data[0] = item;
        return this.data;
    }
}

const arr = new MyArray();
arr.myPush("Carl");
arr.myPush("Melanie");
arr.myPush("Eva");
arr.myPush("Lisa");
console.log(arr.myShift());
console.log(arr);