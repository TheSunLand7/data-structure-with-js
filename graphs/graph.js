/**
 *      2 - 0
 *    / \
 *  1 -  3
 */
//Edge list - primera forma de representar un grafo
const graph = [
    [0, 2],
    [2, 3],
    [2, 1],
    [1, 3],
];

//Adjacent list - segunda forma
const graph2 = [[2], [2, 3], [0, 1, 3], [1, 2]];

const graph3 = new Map();
graph3.set(0, [2]);
graph3.set(1, [2, 3]);
graph3.set(2, [0, 1, 3]);
graph3.set(3, [1, 2]);
console.log(graph3.get(0));

//Adjacent Matrix - tercera forma (con ponderados)
const graph4 = [
    [0, 0, 1, 0],
    [0, 0, 1, 1],
    [1, 1, 0, 1],
    [0, 1, 1, 0],
];
const graph5 = {
    0: [0, 0, 1, 0],
    1: [0, 0, 1, 1],
    2: [1, 1, 0, 1],
    3: [0, 1, 1, 0],
};

/**+++++++++++++++++++++++++++++++++++ */
class MyGraph {
    constructor() {
        this.nodes = 0;
        this.adjacentList = {};
    }
    addVertex(node) {
        this.adjacentList[node] = [];
        this.nodes++;
    }
    addEdge(node1, node2) {
        this.adjacentList[node1].push(node2);
        this.adjacentList[node2].push(node1);
    }
}
const myGraph = new MyGraph();
myGraph.addVertex("1");
myGraph.addVertex("3");
myGraph.addVertex("4");
myGraph.addVertex("5");
myGraph.addVertex("6");
myGraph.addVertex("8");
myGraph.addEdge("8", "4");
myGraph.addEdge("4", "5");
myGraph.addEdge("1", "4");
myGraph.addEdge("5", "3");
myGraph.addEdge("3", "6");
myGraph.addEdge("3", "1");
myGraph.addEdge("1", "6");
console.log(myGraph);

// const grafo = {
//     1: [6, 3, 4],
//     3: [1, 5, 6],
//     4: [1, 5, 8],
//     5: [3, 4],
//     6: [1, 3],
//     8: [4],
// }