class Node {
    constructor(value) {
        this.value = value;
        this.next = null;
    }
}

class MyStack {
    constructor() {
        this.top = null;
        this.bottom = null;
        this.length = 0;
    }
    push(item) {
        //42 --> 12 --> 7 --> 38 --> null
        const newNode = new Node(item);
        if (this.length === 0) {
            this.bottom = newNode;
            this.top = newNode;
        } else {
            let traverse = this.top;
            this.top = newNode;
            this.top.next = traverse;
        }
        this.length++;
        return this;

    }
    peek() {
        return this.top;
    }
    pop() {
        if (this.length > 0) {
            let temp = this.top;
            this.top = temp.next;
            this.length--;
            return temp;
        } else console.log("Empty stack. Cannot delete last item.");
    }
}

const stack = new MyStack();
stack.push(12);
stack.push(7);
stack.push(28);
stack.push(-36);
console.log(stack);
stack.pop();
console.log(stack);