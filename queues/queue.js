class Node {
    constructor(value) {
        this.value = value;
        this.next = null;
    }
}

class MyQueue {
    constructor() {
        this.first = null;
        this.last = null;
        this.length = 0;
    }
    enqueue(item) {
        //42 --> 12 --> 7 --> 38 --> null
        const newNode = new Node(item);
        if (this.length === 0) {
            this.first = newNode;
            this.last = newNode;
        } else {
            this.last.next = newNode;
            this.last = newNode;
        }
        this.length++;
        return this;

    }
    peek() {
        return this.first;
    }
    dequeue() {
        //42 --> 12 --> 7 --> 38 --> null
        if (this.length > 0) {
            let temp = this.first;
            this.first = this.first.next;
            this.length--;
            return temp;
        }
        return "Empty stack. Cannot delete last item.";
    }
}

const myQueue = new MyQueue();
myQueue.enqueue(12);
myQueue.enqueue(7);
myQueue.enqueue(28);
myQueue.enqueue(-36);
console.log(myQueue);
myQueue.pop();
console.log(myQueue);