class Node {
    constructor(value) {
        this.value = value;
        this.next = null;
    }
}

class MySinglyLinkedList {
    constructor(value_) {
        this.head = {
            value: value_,
            next: null,
        }
        this.tail = this.head;
        this.length = 1;
    }

    append(item) {
        //42 --> 7 --> 12 --> null
        const newNode = new Node(item);
        if (this.head.next === null) {
            this.head.next = newNode;
        } else {
            let traverse = this.head;
            while (traverse.next !== null) {
                traverse = traverse.next;
            }
            traverse.next = newNode;
        }
        this.tail.next = newNode;
        this.tail = newNode;
        this.length++;
        return this;
    }

    prepend(item) {
        //42 --> 7 --> 12 --> null
        const newNode = new Node(item);
        newNode.next = this.head;
        this.head = newNode;
        this.length++;
        return this;
    }

    insert(item, index) {
        if (index >= this.length) {
            return this.append(item);
        }
        const newNode = new Node(item);
        let traverse = this.head;
        let count = 0;
        if (index > 0) {
            while (traverse.next !== null) {
                traverse = traverse.next;
                count++;
                if (count === (index - 1)) {
                    newNode.next = traverse.next;
                    traverse.next = newNode;
                    this.length++;
                    return this;
                }
            }
        }
    }

    remove(index) {
        //42 --> 7 --> 12 --> 9 --> null
        if (index > this.length || index < 0) {
            console.log("Please insert a valid index");
            return;
        }
        let traverse = this.head;
        let temp = null;
        let count = 0;
        while (traverse.next !== null) {
            if (count === (index - 1)) {
                temp = traverse.next;
                temp = temp.next;
                delete traverse.next;
                traverse.next = temp;
                this.length--;
                return this;
            }
            traverse = traverse.next;
            count++;
        }
    }
}

const list = new MySinglyLinkedList(42);
list.append(7);
list.append(12);
list.append(-18);
list.append(22);
// list.insert(101, 2);
console.log(list);
list.remove(1);
console.log(list);